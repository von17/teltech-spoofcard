// JavaScript Document

var navBtn, navMenu, mask, bodyElm, main;

(function() {
	if (!navBtn || !mask) {
		navBtn = document.getElementById('mobile-menu');
		mask = document.getElementById('mask');
	}
	
	function init() {
		if (!navMenu || !bodyElm || !main) {
			navMenu = document.getElementById('nav-menu-mobile');
			main = document.getElementById('main-wrapper');
			bodyElm = document.body;
		}
	}
	
	navBtn.addEventListener('click', function() {
		init();
		navMenu.className = mask.className = main.className = bodyElm.className = 'is-active';
	});
	
	mask.addEventListener('click', function() {
		navMenu.className = mask.className = 'not-active';
		bodyElm.className = main.className = '';
	});
})();